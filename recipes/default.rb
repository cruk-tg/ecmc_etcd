#
# Cookbook Name:: ecmc_etcd
# Recipe:: default
#
# Copyright 2016, The University of Edinburgh
#
# All rights reserved - Do Not Redistribute
#
include_recipe 'chef-sugar'
include_recipe 'firewall'

remote_file 'etcd tarball' do
  path "#{Chef::Config[:file_cache_path]}/etcd-tarball.tar.gz"
  source 'https://github.com/coreos/etcd/releases/download/v2.2.5/etcd-v2.2.5-linux-amd64.tar.gz'
  action :create
end

execute 'extract' do
  command "tar xf #{Chef::Config[:file_cache_path]}/etcd-tarball.tar.gz --strip-components=1 -C /usr/local/bin --wildcards '*/etcd' '*/etcdctl'"
  creates '/usr/local/bin/etcd'

  action :run
end

directory '/var/lib/etcd'

template '/etc/init/etcd.conf' do
  source 'etcd.conf.erb'
end

if node['etcd']['initial_cluster_state'] == 'existing'
  template '/etc/init/etcd.override' do
    source 'etcd.override.existing.erb'
    variables({etcd: node['etcd'],ipaddress: node['ipaddress']})
  end
else
  template '/etc/init/etcd.override' do
    source 'etcd.override.erb'
    variables({ipaddress: node['ipaddress']})
  end
end

firewall_rule 'etcd1' do
  port [2379,2380,4001]
  protocol :tcp
  source '172.19.158.0/23'
end

firewall_rule 'etcd2' do
  port [2379,2380,4001]
  protocol :tcp
  source '192.168.99.0/24'
end

service 'etcd' do
 action [:enable, :start]
end
