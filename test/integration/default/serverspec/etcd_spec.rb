require 'serverspec'

set :backend, :exec

describe command("/usr/local/bin/etcdctl -C http://10.0.2.15:2379 member list") do
  its(:exit_status) { should eq 0 }
  its(:stdout) { should include('name=etcd-01')}
end

describe command("/usr/local/bin/etcdctl member list") do
  its(:exit_status) { should eq 0 }
  its(:stdout) { should include('name=etcd-01')}
end
