name             'ecmc_etcd'
maintainer       'The University of Edinburgh'
maintainer_email 'paul.d.mitchell@ed.ac.uk'
license          'All rights reserved'
description      'Installs/Configures etcd for ECMC'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.0.1'

supports "ubuntu"

depends 'chef-sugar'
depends 'firewall'
